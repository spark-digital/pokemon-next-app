import styles from '../styles/PokemonRadioButton.module.css'

function PokemonRadioButton({name, onClickHandler}) {


    return (
        <label onClick={onClickHandler} className={styles.pokemonLabel}>
          <input type="radio" className="nes-radio" name="pokemon" defaultChecked={false}  />
          <span className={styles.name}>{name}</span>
          <i className={`nes-${name}`}></i>
        </label>
    )
}

export default PokemonRadioButton