import { useState } from "react";
import styles from "../../styles/PokemonBattle.module.css";

export default function PokemonBattle({ pokemon }) {
  const [player1, setPlayer1] = useState(pokemon);
  console.log(player1);

const actionsList = pokemon.actions.map((action) =>{
  return (
    <label key ={action.key}>
      <input type="radio" class="nes-radio" name="answer" />
      <span>{action.name}</span>
        {/* <li key={action.key}>{action.name}</li> */}
    </label>
  )
});

  return (
  <main className={styles.container}>
    <article className={styles.player}>
      <h2>Player 1</h2>
      <i className={`nes-${pokemon.type}`}></i>
      <progress className="nes-progress is-success" value="50" max="100"></progress>
      <div className={styles.pokemonPowers}>
        {actionsList}
      </div>
    </article>
    <article className={styles.player}>
      <h2>Player 2</h2>
      <i className={`nes-${pokemon.type}`}></i>
      <div className={styles.pokemonHealth}>
        <progress className="nes-progress is-success" value="50" max="100"></progress>
      </div>
      <div className={styles.pokemonPowers}>
        {actionsList}
      </div>
    </article>
  </main>
  );
}

export async function getServerSideProps(context) {
  const {
    query: { name },
  } = context;

  if (!name) {
    return {
      redirect: "/",
    };
  }

  // NOTE: Fetch pokemon info
  const response = await fetch(
    `http://localhost:3000/api/pokemon?name=${name}`
  );
  const { data } = await response.json();

  return {
    props: {
      pokemon: data,
    },
  };
}
