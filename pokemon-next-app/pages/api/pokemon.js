import data from "./data.js";

export default function handler(req, res) {

    if(req.method === "GET") {
        const { name } = req.query;

        if(!name){
            res.status(404).json({
                message: "No pokemon"
            })
        }

        const pokemonFound = data.find(item =>(item.type===name));

        res.status(200).json({
            data: pokemonFound
        });
    }
}