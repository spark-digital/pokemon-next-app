const POKEMONS = [
  { 
    type: "squirtle",
    actions: [ 
      { key: 'tackle', name: 'Tackle', speed: 2, damage: 20, fn: function(pokemon) { pokemon.receiveDamage(this.damage); } },
      { key: 'water-gun', name: 'Water Gun', speed: 1, damage: 40, fn: function(pokemon) { pokemon.receiveDamage(this.damage); } }, 
      { key: 'rapid-spin', name: 'Rapid Spin', speed: 1, damage: 30, fn: function(pokemon) { pokemon.receiveDamage(this.damage); } }
    ],
  },
  { 
    type: "charmander",
    actions: [ 
      { key: 'scratch', name: 'Scratch', speed: 2, damage: 20, fn: function(pokemon) { pokemon.receiveDamage(this.damage); } },
      { key: 'ember', name: 'Ember', speed: 1, damage: 30, fn: function(pokemon) { pokemon.receiveDamage(this.damage); } }, 
      { key: 'fire-spin', name: 'Fire Spin',speed: 1, damage: 35, fn: function(pokemon) { pokemon.receiveDamage(this.damage); } }
    ],
  },
  { 
    type: "bulbasaur",
    actions: [ 
      { key: 'tackle', name: 'Tackle', speed: 2, damage: 20, fn: function(pokemon) { pokemon.receiveDamage(this.damage); } },
      { key: 'poison-powder', name: 'Poison Powder', speed: 1, damage: 15, fn: function(pokemon) { pokemon.receiveDamage(this.damage); } }, 
      { key: 'razor-leaf', name: 'Razor Leaf', speed: 1, damage: 30, fn: function(pokemon) { pokemon.receiveDamage(this.damage); } }
    ],
  }
];

export default POKEMONS;