import '../styles/globals.css'
import Head from 'next/head';
import "nes.css/css/nes.min.css";

function MyApp({ Component, pageProps }) {
  return <>
  <Head>
        <title>Pokemon game</title>
        <meta name="description" content="Pokemon game" />
        <link rel="icon" href="/favicon.ico" />
        <link rel="stylesheet" href="./node_modules/nes.css/css/nes.min.css"></link>
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin="true" />
        <link href={`https://fonts.googleapis.com/css2?family=Press+Start+2P&display=swap`} rel="stylesheet"></link>
      </Head>
    <Component {...pageProps} />
</>
}
export default MyApp
