import Head from 'next/head'
import { useRouter } from 'next/router';
import styles from '../styles/Home.module.css'
import PokemonRadioButton from '../components/PokemonRadioButton'
export default function Home() {

  const router = useRouter();

  const startBattle = (pokemonName) => {
    router.push(`/pokemon-battle?name=${pokemonName}`);
  }

  const pokemonList = ['squirtle', 'charmander', 'bulbasaur'];

  return (
    <div className={styles.container}>
      <main className={styles.main}>
        <h1 className={styles.title}>
          Choose your Pokemon
        </h1>
        {
          pokemonList.map((pokemon)=>{
            return (
              <PokemonRadioButton key={`pokemon-${pokemon}`} name={pokemon} onClickHandler={() => startBattle(pokemon)}/>
            )
          })
        }
      </main>
    </div>
  )
}
